
var request = require('superagent');
var co = require('co');
var prompt = require('co-prompt');
var moment = require('moment'); //to calculate duration into departure and arrival
var commande;
var arrayCookie; // to store first cookies
var arrayCookie2; // to store the second cookies
var cookie = "";

co(function *() {
do {
    commande = yield prompt(' > ');
    var typeCommande = commande.substring(0,2); //identifiant commande
    switch (typeCommande) {
        case 'AN':
            date = commande.substring(2,7); //extraction date
            origin = commande.substring(7,10); //extraction départ
            destination = commande.substring(10,13); //extraction arrivée
            hour = commande.substring(13,17); //extraction heure
            console.log(commande);
            uri = 'http://localhost:8080/api/flight/all/' + date + '/' + origin + '/' + destination + '/' + hour;
                // PARTIE QUI MARCHE
                request
                .get(uri)
                .set('Accept', 'application/json')
                .end(function(err, res)
                {
                    if (err || !res.ok)
                    {
                        var output = JSON.stringify(err); //on écrit l'erreur envoyée par serveur
                        //output = output.replace(/"/g,""); //on enlève les guillemets
                        console.log(output);
                    }
                    else
                    {
                        flights = res.body;
                        for (var i = 0; i < flights.length; i++)
                        {
                            lineDisplayed = i + 1;
                            company = flights[i].idCompany;
                            numberFlight = flights[i].numberFlight;
                            departure = flights[i].airportDeparture;
                            arrival = flights[i].airportArrival;
                            availabilityJ = flights[i].availabilityJ;
                            availabilityY = flights[i].availabilityY;
                            availabilityM = flights[i].availabilityM;
                            hourDeparture = flights[i].hourDeparture;
                            hourArrival = flights[i].hourArrival;
                            dateFlight = flights[i].dateFlight;
                            dayFlight = dateFlight.substring(0,2);
                            monthFlight = dateFlight.substring(2,5);
                            // duration calculation
                            t1 = moment(hourDeparture, "hhmm"); //departure conversion
                            t2 = moment(hourArrival, "hhmm"); //arrival conversion
                            duration = moment(t2.diff(t1)).format("hh:mm");
                            // display like asking on subject
                            console.log('** RSI FLIGHT AVAILABILITY SYSTEM - AN ** ' + dayFlight + ' ' + monthFlight + ' ' + new Date().getFullYear());
                            console.log(lineDisplayed + '      ' +  company + ' ' + numberFlight + ' J' + availabilityJ + ' Y' + availabilityY + ' M'+ availabilityM + '    ' + departure + ' ' + arrival + '      ' + hourDeparture + '     ' + hourArrival + '     ' + duration);
                            break;
                        }
                        // STORE COOKIES
                        arrayCookie = res.headers['set-cookie'];
                        //console.log(arrayCookie);
                    }
                });
            break;
        case 'SS':
            numberSeats = commande.substring(2,3); //extraction nombre sièges
            classSeats = commande.substring(3,4); //extraction classe sièges
            lineDisplayed = commande.substring(4,5); //extraction ligne affichée pendant recherche

            for(var i=0; i < arrayCookie.length; i++) // to pass cookies by the header
            {
                if(cookie == "")
                    cookie = arrayCookie[0]; //initialisation du stockage des cookies
                else
                    cookie = cookie + ";" + arrayCookie[i];
            }

            if (arrayCookie2 != undefined) {

                uri = 'http://localhost:8080/api/flight/reservation/' + reservationNumber;
                parameters = { numberOfSeats: numberSeats, seatClass: classSeats };
                cookie = "";
                for(var i=0; i < arrayCookie2.length; i++) // to pass cookies by the header
                {
                    if(cookie == "")
                        cookie = arrayCookie2[0]; //initialisation du stockage des cookies
                    else
                        cookie = cookie + ";" + arrayCookie2[i];
                }
                // le client envoie une requête pour créer les sièges
                request.post(uri)
                .send(parameters)
                .set('Accept', 'application/json')
                .set('Cookie', cookie) //passing cookies
                .end(function(err, res)
                {
                    if (err || !res.ok) {
                        console.log(err);
                        flagCommande = true;
                    }
                    else
                    {
                        volRetourne = res.body;
                        arrayReservations = volRetourne.reservation;
                        for(var i=0; i < arrayReservations.length; i++ ){
                            if(arrayReservations[i]._id == reservationNumber)
                            {
                                arraySeats = arrayReservations[i].seat;
                                break;
                            }
                        }
                        flagY = false;
                        flagM = false;
                        flagJ = false;
                        lineDisplayed = 0;
                        // boucle pour écrire les sièges
                        for (var i=0; i < arraySeats.length; i++ )
                        {
                            seatNumber = 0;
                            seat = arraySeats[i];
                            classActuelle = seat.class;
                            switch(classActuelle)
                            {
                                case 'Y' :
                                    if(!flagY){
                                        for(var i in arraySeats){ //on compte les sièges de la même classe
                                            seat = arraySeats[i];
                                            if(seat.class == 'Y')
                                            {
                                                seatNumber++;
                                            }
                                        }
                                        lineDisplayed++;
                                        company = volRetourne.idCompany;
                                        numberFlight = volRetourne.numberFlight;
                                        classSeats = classActuelle;
                                        departure = volRetourne.airportDeparture;
                                        arrival = volRetourne.airportArrival;
                                        hourDeparture = volRetourne.hourDeparture;
                                        hourArrival = volRetourne.hourArrival;
                                        dateFlight = volRetourne.dateFlight;
                                        console.log(lineDisplayed +' ' + company + ' ' + numberFlight + ' ' + classSeats + ' ' + dateFlight + ' ' + seatNumber + ' ' + departure + arrival + ' ' + hourDeparture + ' ' + hourArrival );
                                    }
                                    flagY = true;
                                    break;
                                case 'M' :
                                    if(!flagM){
                                        for(var i in arraySeats){ //on compte les sièges de la même classe
                                            seat = arraySeats[i];
                                            if(seat.class == 'M')
                                            {
                                                seatNumber++;
                                            }
                                        }
                                        lineDisplayed++;
                                        company = volRetourne.idCompany;
                                        numberFlight = volRetourne.numberFlight;
                                        classSeats = classActuelle;
                                        departure = volRetourne.airportDeparture;
                                        arrival = volRetourne.airportArrival;
                                        hourDeparture = volRetourne.hourDeparture;
                                        hourArrival = volRetourne.hourArrival;
                                        dateFlight = volRetourne.dateFlight;
                                        console.log(lineDisplayed +' ' + company + ' ' + numberFlight + ' ' + classSeats + ' ' + dateFlight + ' ' + seatNumber + ' ' + departure + arrival + ' ' + hourDeparture + ' ' + hourArrival );
                                    }
                                    flagM = true; break;
                                case 'J' :
                                    if(!flagJ){
                                        for(var i in arraySeats){ //on compte les sièges de la même classe
                                            seat = arraySeats[i];
                                            if(seat.class == 'J')
                                            {
                                                seatNumber++;
                                            }
                                        }
                                        lineDisplayed++;
                                        company = volRetourne.idCompany;
                                        numberFlight = volRetourne.numberFlight;
                                        classSeats = classActuelle;
                                        departure = volRetourne.airportDeparture;
                                        arrival = volRetourne.airportArrival;
                                        hourDeparture = volRetourne.hourDeparture;
                                        hourArrival = volRetourne.hourArrival;
                                        dateFlight = volRetourne.dateFlight;
                                        console.log(lineDisplayed +' ' + company + ' ' + numberFlight + ' ' + classSeats + ' ' + dateFlight + ' ' + seatNumber + ' ' + departure + arrival + ' ' + hourDeparture + ' ' + hourArrival );
                                    }
                                    flagJ = true;
                                    break;
                            }
                        }
                    }
                });
            }
            else {
                uri = 'http://localhost:8080/api/flight/reservation/';
                parameters = { numberOfSeats: numberSeats, seatClass: classSeats , displayedLineNumber: lineDisplayed };

                request
                .post(uri)
                .set('Accept', 'application/json')
                .set('Cookie', cookie) //passing cookies
                .send(parameters)
                .end(function(err, res)
                {
                    if (err || !res.ok)
                    {
                        var output = JSON.stringify(res.text); //on écrit l'erreur envoyée par serveur
                        output = output.replace(/"/g,""); //on enlève les guillemets
                        console.log(output);
                    }
                    else
                    {
                        // on créé et récupère le numéro de réservation
                        // EXTRACT COOKIES
                        if (arrayCookie2 == undefined)
                            arrayCookie2 = res.headers['set-cookie'];
                        // extract reservation number
                        reservationCookie = arrayCookie2[0].split(';')
                        reservationCookie = reservationCookie[0].split(',');
                        reservationCookie = reservationCookie[0].split('=');
                        reservationNumber = reservationCookie[1];
                        cookie = "";
                        for(var i=0; i < arrayCookie2.length; i++) // to pass cookies by the header
                        {
                            if(cookie == "")
                                cookie = arrayCookie2[0]; //initialisation du stockage des cookies
                            else
                                cookie = cookie + ";" + arrayCookie2[i];
                        }
                        // le client renvoie une requête pour créer les sièges
                        uri = 'http://localhost:8080/api/flight/reservation/' + reservationNumber;
                        request.post(uri)
                        .send({ numberOfSeats: numberSeats, seatClass: classSeats })
                        .set('Accept', 'application/json')
                        .set('Cookie', cookie) //passing cookies
                        .end(function(err, res)
                        {
                            if (err || !res.ok) {
                                console.log(err);
                                flagCommande = true;
                            }
                            else
                            {
                                volRetourne = res.body;
                                arrayReservations = volRetourne.reservation;
                                for(var i=0; i < arrayReservations.length; i++ ){
                                    if(arrayReservations[i]._id == reservationNumber)
                                    {
                                        arraySeats = arrayReservations[i].seat;
                                        break;
                                    }
                                }
                                flagY = false;
                                flagM = false;
                                flagJ = false;
                                lineDisplayed = 0;
                                // boucle pour écrire les sièges
                                for (var i=0; i < arraySeats.length; i++ )
                                {
                                    seatNumber = 0;
                                    seat = arraySeats[i];
                                    classActuelle = seat.class;
                                    switch(classActuelle)
                                    {
                                        case 'Y' :
                                            if(!flagY){
                                                for(var i in arraySeats){ //on compte les sièges de la même classe
                                                    seat = arraySeats[i];
                                                    if(seat.class == 'Y')
                                                    {
                                                        seatNumber++;
                                                    }
                                                }
                                                lineDisplayed++;
                                                company = volRetourne.idCompany;
                                                numberFlight = volRetourne.numberFlight;
                                                classSeats = classActuelle;
                                                departure = volRetourne.airportDeparture;
                                                arrival = volRetourne.airportArrival;
                                                hourDeparture = volRetourne.hourDeparture;
                                                hourArrival = volRetourne.hourArrival;
                                                dateFlight = volRetourne.dateFlight;
                                                console.log(lineDisplayed +' ' + company + ' ' + numberFlight + ' ' + classSeats + ' ' + dateFlight + ' ' + seatNumber + ' ' + departure + arrival + ' ' + hourDeparture + ' ' + hourArrival );
                                            }
                                            flagY = true;
                                            break;
                                        case 'M' :
                                            if(!flagM){
                                                for(var i in arraySeats){ //on compte les sièges de la même classe
                                                    seat = arraySeats[i];
                                                    if(seat.class == 'M')
                                                    {
                                                        seatNumber++;
                                                    }
                                                }
                                                lineDisplayed++;
                                                company = volRetourne.idCompany;
                                                numberFlight = volRetourne.numberFlight;
                                                classSeats = classActuelle;
                                                departure = volRetourne.airportDeparture;
                                                arrival = volRetourne.airportArrival;
                                                hourDeparture = volRetourne.hourDeparture;
                                                hourArrival = volRetourne.hourArrival;
                                                dateFlight = volRetourne.dateFlight;
                                                console.log(lineDisplayed +' ' + company + ' ' + numberFlight + ' ' + classSeats + ' ' + dateFlight + ' ' + seatNumber + ' ' + departure + arrival + ' ' + hourDeparture + ' ' + hourArrival );
                                            }
                                            flagM = true; break;
                                        case 'J' :
                                            if(!flagJ){
                                                for(var i in arraySeats){ //on compte les sièges de la même classe
                                                    seat = arraySeats[i];
                                                    if(seat.class == 'J')
                                                    {
                                                        seatNumber++;
                                                    }
                                                }
                                                lineDisplayed++;
                                                company = volRetourne.idCompany;
                                                numberFlight = volRetourne.numberFlight;
                                                classSeats = classActuelle;
                                                departure = volRetourne.airportDeparture;
                                                arrival = volRetourne.airportArrival;
                                                hourDeparture = volRetourne.hourDeparture;
                                                hourArrival = volRetourne.hourArrival;
                                                dateFlight = volRetourne.dateFlight;
                                                console.log(lineDisplayed +' ' + company + ' ' + numberFlight + ' ' + classSeats + ' ' + dateFlight + ' ' + seatNumber + ' ' + departure + arrival + ' ' + hourDeparture + ' ' + hourArrival );
                                            }
                                            flagJ = true;
                                            break;
                                    }
                                }
                            }
                        });
                    }
                });
            }
            break;
        case 'NM':
            var patternNom = /^NM1[A-Z]+\//;
            var patternPrenom = /\/[A-Z]+\s/;
            var patternGenre = /\s(MS|MR)$/ ;

            var nom = commande.match(patternNom);
            nom = nom[0].substring(3,nom[0].length - 1);

            var prenom = commande.match(patternPrenom);
            prenom = prenom[0].substring(1,prenom[0].length - 1);

            var genre = commande.match(patternGenre);
            genre = genre[0].substring(1, genre[0].length);

            uri = 'http://localhost:8080/api/flight/reservation/' + reservationNumber;
            parameters = { firstName: prenom, lastName: nom, gender: genre };
            cookie = "";

            for(var i=0; i < arrayCookie2.length; i++) // to pass cookies by the header
            {
                if(cookie == "")
                    cookie = arrayCookie2[0]; //initialisation du stockage des cookies
                else
                    cookie = cookie + ";" + arrayCookie2[i];
            }

            // le client envoie une requête pour ajouter les noms
            request.patch(uri)
            .send(parameters)
            .set('Accept', 'application/json')
            .set('Cookie', cookie) //passing cookies
            .end(function(err, res)
            {
                if (err || !res.ok) {
                    console.log(err);
                    flagCommande = true;
                }
                else
                {
                    volRetourne = res.body;
                    arrayReservations = volRetourne.reservation;
                    var i, indiceAAfficher, reservation;
                    // On parcourt les réservations pour trouver la notre.
                    for(i=0; i < arrayReservations.length; i++ ){
                        if(arrayReservations[i]._id == reservationNumber)
                        {
                            reservation = arrayReservations[i];
                            break;
                        }
                    }

                    // Maintenant qu'on a notre réservation, on va chercher les sièges et afficher les noms des passagers
                    arraySeats = reservation.seat;

                    indiceAAfficher = 1;
                    for (i = 0; i < arraySeats.length; i++) {
                        if (arraySeats[i].lastNamePassenger != undefined) {
                            console.log(indiceAAfficher + '. ' + arraySeats[i].lastNamePassenger + '/' + arraySeats[i].firstNamePassenger + ' ' + arraySeats[i].genderPassenger);
                            indiceAAfficher++;
                        }
                    }

                    // Maintenant qu'on a affiché les noms des passagers, on peut afficher les infos des places et du vol
                    var nombreSiegeJ, nombreSiegeM, nombreSiegeY;
                    nombreSiegeJ = nombreSiegeM = nombreSiegeY = 0 ;
                    for (i = 0; i < arraySeats.length; i++) {
                        switch (arraySeats[i].class) {
                            case 'J':
                                nombreSiegeJ++;
                                console.log(indiceAAfficher +'. ' + volRetourne.idCompany + ' ' + volRetourne.numberFlight + ' ' + arraySeats[i].class + ' ' + volRetourne.dateFlight + ' ' + nombreSiegeJ + ' ' + volRetourne.airportDeparture + volRetourne.airportArrival + ' ' + volRetourne.hourDeparture + ' ' + volRetourne.hourArrival );
                                break;
                            case 'M':
                                nombreSiegeM++;
                                console.log(indiceAAfficher +'. ' + volRetourne.idCompany + ' ' + volRetourne.numberFlight + ' ' + arraySeats[i].class + ' ' + volRetourne.dateFlight + ' ' + nombreSiegeM + ' ' + volRetourne.airportDeparture + volRetourne.airportArrival + ' ' + volRetourne.hourDeparture + ' ' + volRetourne.hourArrival );
                                break;
                            case 'Y':
                                nombreSiegeY++;
                                console.log(indiceAAfficher +'. ' + volRetourne.idCompany + ' ' + volRetourne.numberFlight + ' ' + arraySeats[i].class + ' ' + volRetourne.dateFlight + ' ' + nombreSiegeY + ' ' + volRetourne.airportDeparture + volRetourne.airportArrival + ' ' + volRetourne.hourDeparture + ' ' + volRetourne.hourArrival );
                                break;
                        }
                        indiceAAfficher++;
                    }
                }
            });
            break;
        case 'FX':
            console.log('Vous êtes en mode calcul de prix');
            break;
        case 'TQ':
            console.log('Vous êtes en mode visualisation du ticket');
            break;
        case 'TT':
            console.log('TTP');
            uri = 'http://localhost:8080/api/flight/reservation/' + reservationNumber;
            cookie = "";

            for(var i=0; i < arrayCookie2.length; i++) // to pass cookies by the header
            {
                if(cookie == "")
                    cookie = arrayCookie2[0]; //initialisation du stockage des cookies
                else
                    cookie = cookie + ";" + arrayCookie2[i];
            }

            request.put(uri)
            .set('Accept', 'application/json')
            .set('Cookie', cookie) //passing cookies
            .end(function(err, res)
            {
                if (err || !res.ok) {
                    console.log(err);
                    flagCommande = true;
                }
                else
                {
                    volRetourne = res.body;
                    arrayReservations = volRetourne.reservation;

                    // On parcourt les réservations pour trouver la notre.
                    for(i=0; i < arrayReservations.length; i++ ){
                        if(arrayReservations[i]._id == reservationNumber)
                        {
                            reservation = arrayReservations[i];
                            break;
                        }
                    }

                    // Maintenant qu'on a notre réservation, on va chercher les sièges et afficher les noms des passagers
                    arraySeats = reservation.seat;

                    indiceAAfficher = 1;
                    for (i = 0; i < arraySeats.length; i++) {
                        if (arraySeats[i].lastNamePassenger != undefined) {
                            console.log(indiceAAfficher + '. ' + arraySeats[i].lastNamePassenger + '/' + arraySeats[i].firstNamePassenger + ' ' + arraySeats[i].genderPassenger);
                            indiceAAfficher++;
                        }
                    }

                    // Maintenant qu'on a affiché les noms des passagers, on peut afficher les infos des places et du vol
                    nombreSiegeJ = nombreSiegeM = nombreSiegeY = 0 ;
                    for (i = 0; i < arraySeats.length; i++) {
                        switch (arraySeats[i].class) {
                            case 'J':
                                nombreSiegeJ++;
                                console.log(indiceAAfficher +'. ' + volRetourne.idCompany + ' ' + volRetourne.numberFlight + ' ' + arraySeats[i].class + ' ' + volRetourne.dateFlight + ' ' + nombreSiegeJ + ' ' + volRetourne.airportDeparture + volRetourne.airportArrival + ' ' + volRetourne.hourDeparture + ' ' + volRetourne.hourArrival );
                                break;
                            case 'M':
                                nombreSiegeM++;
                                console.log(indiceAAfficher +'. ' + volRetourne.idCompany + ' ' + volRetourne.numberFlight + ' ' + arraySeats[i].class + ' ' + volRetourne.dateFlight + ' ' + nombreSiegeM + ' ' + volRetourne.airportDeparture + volRetourne.airportArrival + ' ' + volRetourne.hourDeparture + ' ' + volRetourne.hourArrival );
                                break;
                            case 'Y':
                                nombreSiegeY++;
                                console.log(indiceAAfficher +'. ' + volRetourne.idCompany + ' ' + volRetourne.numberFlight + ' ' + arraySeats[i].class + ' ' + volRetourne.dateFlight + ' ' + nombreSiegeY + ' ' + volRetourne.airportDeparture + volRetourne.airportArrival + ' ' + volRetourne.hourDeparture + ' ' + volRetourne.hourArrival );
                                break;
                        }
                        indiceAAfficher++;
                    }
                }
            });
            break;
        case 'q':
            console.log('A bientôt sur Zeruzkan');
            break;
        default: console.log('Syntaxe incorrecte. \n Pour quitter appuyez sur q ou CTRL-C');
    }
} while (commande != 'q');

// end command
process.exit();

    // request
    //    .get('http://localhost:8080/api/')
    //    .set('Accept', 'application/json')
    //    .end(function (err, res) {
    //        var str = res.text;
    //        var obj = JSON.parse(str);

    //         console.log(obj);
    //    });

});
