// server.js

//EXPRESS PACKAGE
// =============================================================================
var express    = require('express');        // call express
var app        = express();                 // define our app using express
global.cookieParser = require('cookie-parser'); // call cookie parser
app.use(cookieParser());

var router     = express.Router();          // get an instance of the express Router
var path       = require('path');

//BODY PARSER
// =============================================================================
global.bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));// configure app to use bodyParser()
app.use(bodyParser.json()); // this will let us get the data from a POST

// CONNECT DATABASE
// =============================================================================
global.mongoose = require('mongoose'); // load mongoose package
//var Flight = mongoose.model('Flight', flightSchema);
mongoose.Promise = global.Promise; // Use native Node promises to catch asynchronous events
mongoose.connect('mongodb://localhost/zeruzkanDB') // connect to MongoDB
.then(() =>  console.log('Connected to zeruzkanDB'))
.catch((err) => console.error(err));

// LOAD MODEL
// =============================================================================
require('zeruzkanScheme')(mongoose); //call external file to load data model

//ROUTES
// =============================================================================
require('zeruzkanRouter')(app, router, path); //use router external file with app and router in parameter

// START THE SERVER
// =============================================================================
var port = process.env.PORT || 8080;        // set our port
app.listen(port);
console.log('Serveur Zeruzkan working on port ' + port);

// CONVERT FLIGHT.DAT TO MONGO OBJECT
// =============================================================================
 require('zeruzkanScriptDatMongo')(mongoose); //call external file to convert dat to mongodb
